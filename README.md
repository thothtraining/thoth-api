Thoth PHP API v1.0.0
================

API for Thoth Training Center written in PHP

Check out the Wiki for more information on how to use the API.

(note: You will need an API Auth key from Thoth in order to use this API)

We support composer! https://packagist.org/packages/thoth/thoth-api

Add this to your composer.json file:
```
"thoth/thoth-api": "dev-master"
```